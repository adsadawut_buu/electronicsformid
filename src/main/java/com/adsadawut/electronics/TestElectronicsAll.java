/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.electronics;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TestElectronicsAll {
    public static void main(String[] args) {
        Electronics electronic1 = new Electronics("LG","black"); //ลืม commit ว่าสร้างObjนี้ครับ
        electronic1.OnOff('i'); //เช็คว่าเมทอด สามารถใช้เปิดใช้งานได้หรือไม่?
        
        electronic1.checkStatus(); //เช็ค obj ว่าเปิดอยู่หรือไม่?
        
        electronic1.OnOff('o');
        electronic1.checkStatus(); 
        
        TV tv1= new TV("Sony","gray");
        tv1.OnOff('i');
        tv1.checkStatus();
        
        tv1.Move('w');
        System.out.println(tv1);
        tv1.Move('s');
        tv1.Move('s');
        System.out.println(tv1);
        
        tv1.Move(0);
        System.out.println(tv1);
        tv1.Move(101);
        System.out.println(tv1);
    }
}
