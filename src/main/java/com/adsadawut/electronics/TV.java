/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.electronics;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TV extends Electronics { //ใช้ extends เสมอเพื่ออ้างถึง Electronics หรือ Class อื่นๆที่ต้องการ 
    private int ch; //ช่อง 1-100
    private char direction; //ปุ่มกดย้ายช่อง
    private int lastCh = 0;
    
    public TV(String brand,String color){
        super(brand,color); //ใช้ ฟังชันก์ super() เพื่อเรียกใช้ constructor ของ Class อื่นเสมอ
    }
    
    public boolean Move(char direction){
        //ปุ่ม w คือเลื่อนช่องไป 1 ช่อง
        if(direction == 'w'){
            lastCh+=1;
            if(lastCh<0 ||lastCh >100){
                System.out.println("Can't move next to Ch!!!");
                lastCh-=1;
                return false;
            }
             //
        }else if(direction=='s'){ //ปุ่ม s คือ ย้อนกลับไป 1 ช่อง
            lastCh-=1;
            if(lastCh<0 ||lastCh >100){
                System.out.println("Can't move back to Ch!!!");
                lastCh+=1;
                return false;
            }
             
        } 
        return true;
    }
    
    public boolean Move(int ch){
        if(ch>100){ 
            System.out.println("Unknow This Ch!!!");
            return false;
        }
        lastCh= ch;
        return true;
    }
    
    @Override
    public String toString(){
        return "TV channel is "+lastCh;
    }
}
