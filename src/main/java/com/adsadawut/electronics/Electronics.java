/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.electronics;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Electronics {

    protected String color;
    protected String brand;
    protected char switchs;
    
    
    public Electronics(String brand, String color){
        this.brand = brand;
        this.color = color;
        
    }
   
    public boolean OnOff(char switchs){
        this.switchs = switchs;
        switch (switchs){
            case 'i':
                System.out.println("Electonic On");
                break;
            case 'o':
                System.out.println("Electonis Off");
                break;
        }            
        return true;
    }
    
    public void checkStatus(){
        if(switchs == 'i'){
            System.out.println("Electronic : "+this.brand +" Color : "+this.color+" Is On");
        }else if(switchs == 'o'){
            System.out.println("Electronic : "+this.brand +" Color : "+this.color+" Is Off");
        }
    }
    
    public String toString(){
            return "Electronic : "+this.brand +"Color : "+this.color;
    }
    
    public String getColor() {
        return color;
    }

    public String getBrand() {
        return brand;
    }
}
